This is a sample project, wrapping the sentiment prediction model provided in a web simple application.

## Short overview


Because I didn't want to send a ton of time on this, I decided to make this into the type of application I'm most comfortable with right now due to my job. So I've made a simple web app around Django.

The app is setup like this:

To simplify things, we keep just one django app (and repository) for the whole project. We deploy this app in 3 flavours. We run web, fetcher and predictor instances.

* The web instance is a standard Django web application that exposes a view that returns a csv containing all the required information for a number of tweets.

* The fether instance runs a Django command. This command is on an endless loop and continuously queries twitter for newer tweets and records them in the database.

* The predictor instance runs another Django command. This command is also on an endless loop and on each run of that loop fetches all tweets that we haven't generated predictions on from the database and runs the prediction logic for them.

The idea here was that each of these 3 apps could scale independently ie. we could run multiple instances of the predictor instance if we have a heavy model or a ton of tweets, more web instances if we're serving this data in different ways to a lot of consumers etc.


## Code organization

All our application is set up around a relational database, Postgres specifically but that doesn't matter too much. Under twisent/models.py you'll find the Django model class that represents the single DB table we use. This table is used by all 3 different instance types of this application.

 Then, the code for serving the csv view lives under twisent/views/csv_view.py.

 The code for the fetcher lives under twisent/management/commands/fetch_tweets.py and twisent/services/tweet_fetching_service.py.

 The code for the predictor lives under twisent/management/commands/predict_sentiment.py and twisent/services/prediciton_service.py


 Under django-app/settings.py we have a few settings that we decided should be more easily configurable eg. the tags we care about, the fetch and predict intervals and the location of the model and word index.


 Finally we have some tests for all of these under twiset/test/*. Note that this isn't a comprehensive test suite as I didn't want to spend a lot of time testing around edgecases. So we're testing mostly the happy path here. In a real system we'd also have tests testing all the edge cases.

## Local dev environment

I went with a dockerized set up that brings up our services and a PostgreSQL database.


# Important:

Make sure to set up your twitter keys under .env.dev otherwise you won't be able to fetch tweets.

Simply running

```
docker-compose build
docker-compose up
```

should build the images and bring up the 4 containers.

On startup, the predictor service will run our provided entrypoint.sh file, which will take care of the following:

* Create and run any database migrations we're missing.
* Download the model and w2i files if they're not already at the expected directory.

If you need to clean up the development database, just uncomment the flush command under the entrypoint.sh file.

Running the tests is as simple as executing

```
docker-compose run web python manage.py test
```

Clean local deployment with:

```
docker-compose down --rmi all --volumes
```


You can see either inspect the database directly to see that the process is going well (easy way to do so, attach to the container, run python mnage.py shell and use the Django ORM).
Or you can go to http://127.0.0.1:8001/twisent/csv?number=100 and get a csv with the number of results you're expecting.

