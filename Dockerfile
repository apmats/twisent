FROM python:3.7

WORKDIR /usr/src/twisent

COPY requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ENV PYTHONUNBUFFERED 1

COPY . .
