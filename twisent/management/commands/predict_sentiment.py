from django import db
from django.core.management.base import BaseCommand
from django.db import transaction


from twisent.models import Tweet
from twisent.services.prediction_service import PredictionService
import time
from django.conf import settings

import logging

logger = logging.getLogger('predict_daemon')


class Command(BaseCommand):
    def handle(self, *args, **options):
        while True:
            try:
                Command._send_unprocessed_tweets_for_prediction()
            except db.DatabaseError as ex:
                logger.error('Unexpected exception while processing tweets', ex)
                pass
            except Exception as ex:
                logger.error('Unexpected exception while processing tweets', ex)
                raise ex
            time.sleep(settings.PROCESSING_INTERVAL_SECONDS)

    @staticmethod
    def _send_unprocessed_tweets_for_prediction():
        with transaction.atomic():
            # Acquire a db lock on these tweets so any other worker
            # will block here
            unprocessed_tweet_ids = list(Tweet.objects.filter(
                status=Tweet.Status.PENDING
            ).select_for_update(nowait=True).values_list('id', flat=True))
            # Prevent other processes from blocking on your locked set
            # afterwards by marking them as processed
            # (as any worker will try to lock only unprocessed rows)
            unprocessed_tweets = Tweet.objects.filter(id__in=
                                                      unprocessed_tweet_ids)
            unprocessed_tweets.update(status=Tweet.Status.PROCESSING)
            for tweet in unprocessed_tweets:
                tweet.sentiment = PredictionService.predict(tweet.text)
                tweet.status = Tweet.Status.PROCESSED
                tweet.save()

