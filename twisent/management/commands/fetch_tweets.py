from datetime import datetime, timedelta

from django import db
from django.core.management.base import BaseCommand
from django.db import transaction
from django.conf import settings
from django.utils import timezone


import time

import logging


from twisent.services.tweet_fetching_service import TweetFetchingService

from twisent.models import Tweet


logger = logging.getLogger('fetch_daemon')


class Command(BaseCommand):
    def handle(self, *args, **options):
        while True:
            try:
                after_id = Tweet.objects.all().order_by('-tweet_id').values_list('tweet_id', flat=True).first()
                after_date = datetime.today().date() - timedelta(days=1)
                TweetFetchingService.fetch_tweets(tags_of_interest=settings.TAGS_OF_INTEREST,
                                          after_date=after_date,
                                          after_id=after_id,
                                          num_to_fetch=settings.TWEETS_PER_FETCH)
            except Exception as ex:
                logger.error('Unexpected error while fetching tweets', ex)
                raise ex
            time.sleep(settings.FETCHING_INTERVAL_SECONDS)
