import logging
from datetime import date
from typing import List, Optional

import tweepy
from django import db
from django.conf import settings
from twisent.models import Tweet
from typeguard import typechecked

logger = logging.getLogger('fetcher')


@typechecked
class TweetFetchingService:

    @staticmethod
    def fetch_tweets(tags_of_interest: List[str], after_date: Optional[date],
                     after_id: Optional[str],
                     num_to_fetch: Optional[int] = 1000) -> None:
        query_string = ' OR '.join(tags_of_interest)
        auth = tweepy.OAuthHandler(settings.TWITTER_API_KEY,
                                   settings.TWITTER_API_SECRET_KEY)
        auth.set_access_token(settings.TWITTER_ACCESS_TOKEN,
                              settings.TWITTER_ACCESS_TOKEN_SECRET)
        api = tweepy.API(auth, wait_on_rate_limit=True)

        for tweet in tweepy.Cursor(api.search, q=query_string,
                                   lang="en",
                                   since=after_date,
                                   since_id=after_id).items(num_to_fetch):
            # Failing to save a tweet should be recoverable.
            # Since we have a unique id constraint
            # this can arise easily if the API returns us
            # a tweet twice, or we have multiple fetchers running
            # so we handle this here.
            try:
                Tweet(text=tweet.text, created_at=tweet.created_at,
                      tweet_id=tweet.id).save()
            except db.DatabaseError as er:
                logger.warning(
                    'Failed to save tweet with id:{}'.format(tweet.id), er)
