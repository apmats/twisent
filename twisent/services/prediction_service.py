import json
import logging
import os
import re
from numbers import Real
from typing import List, Dict

from django.conf import settings
from keras.models import load_model
from nltk import TweetTokenizer
from typeguard import typechecked
import tensorflow as tf


logger = logging.getLogger('predictor')


@typechecked
class PredictionService:
    tokenizer = TweetTokenizer(preserve_case=False)
    w2i = json.loads(
        open(os.path.join(settings.BASE_DIR, settings.W2I_LOCATION)).read())
    model = load_model(os.path.join(settings.BASE_DIR, settings.MODEL_LOCATION))
    out_of_dict_token = '*#*UNK*#*'

    @staticmethod
    def _transform_tokenized_to_input(w2i: Dict[str, int],
                                      list_of_tokens: List[str]) -> List[int]:
        res = []
        for token in list_of_tokens:
            mapping = w2i.get(token, w2i[PredictionService.out_of_dict_token])
            res.append(mapping)
        return res

    @staticmethod
    def _replace_reserved_sequences_with_tokens(string: str) -> str:
        regex_to_replacement = [
            ([
                 '(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]'
                 '[a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]'
                 '[a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|'
                 '(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|'
                 'www\.[a-zA-Z0-9]+\.[^\s]{2,})'], '<url>'),
            (['@\w*'], '<user>'),
            (['[-+]?((\d+(\.\d*)?)|(\.\d*))'], '<number>'),
            ([':-?\)'], '<smileface>'),
            ([':-?\('], '<sadface>'),
            ([':-?D'], '<lolface>'),
            ([':-?\|'], '<neutralface>'),
            (['#\w*'], '<hashtag>')
        ]
        for (list_of_regexes, replacement) in regex_to_replacement:
            for regex in list_of_regexes:
                string = re.sub(regex, replacement, string)
        return string

    @staticmethod
    def predict(tweet: str) -> Real:
        cleaned_up_text = PredictionService.\
            _replace_reserved_sequences_with_tokens(tweet)
        tokens = PredictionService.tokenizer.tokenize(cleaned_up_text)
        model_input = PredictionService._transform_tokenized_to_input(
            PredictionService.w2i, tokens)
        sentiment = PredictionService.model.predict(
            tf.convert_to_tensor([model_input], dtype=tf.int64))[0][0]
        return sentiment
