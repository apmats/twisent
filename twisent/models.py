from django.db import models


class Tweet(models.Model):
    class Status(models.TextChoices):
        PENDING = 'pending'
        PROCESSING = 'processing'
        PROCESSED = 'processed'

    tweet_id = models.TextField(max_length=100, unique=True)
    text = models.TextField(max_length=500)
    sentiment = models.FloatField(null=True)
    created_at = models.DateTimeField(editable=False)
    status = models.CharField(
        max_length=30,
        choices=Status.choices,
        default=Status.PENDING
    )
    updated_at = models.DateTimeField(
        auto_now=True, editable=False
    )

