from django.views import View
from django.http import HttpResponse
from twisent.models import Tweet
import csv


class CSVDownloadView(View):

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="file.csv"'
        number = int(self.request.GET.get('number', '1000'))
        writer = csv.writer(response, quoting=csv.QUOTE_ALL)
        # This is okay to be here for now, we could extract this into a
        # service if the requirements become more complex and the
        # filtering logic gets complicated.
        # Right now, having a service just for this behavior would
        # complicate things.
        for tweet in Tweet.objects.filter(status=Tweet.Status.PROCESSED).\
            values_list('tweet_id', 'text', 'created_at', 'sentiment')\
                .order_by('-updated_at')[:number]:
            writer.writerow(tweet)
        return response
