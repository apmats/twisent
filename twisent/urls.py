from django.urls import path

from twisent.views.csv_view import CSVDownloadView

urlpatterns = [
    path('csv', CSVDownloadView.as_view(), kwargs={'number': 1000}, name='csv'),
]