import csv
import io
from datetime import datetime

from django.test import TestCase
from django.urls import reverse
from twisent.models import Tweet


class CsvViewTest(TestCase):

    def setUp(self):
        self.tweet1 = Tweet(text='foo bar', tweet_id='123',
                            created_at=datetime.now(),
                            sentiment=1.0,
                            status=Tweet.Status.PROCESSED)
        self.tweet1.save()
        self.tweet2 = Tweet(text='bla bla', tweet_id='456',
                            created_at=datetime.now(),
                            sentiment=0.5,
                            status=Tweet.Status.PROCESSED)
        self.tweet2.save()

    def test_fetch_with_default_1000_limit(self):
        resp = self.client.get(reverse('csv'))
        self.assertEqual(resp.status_code, 200)
        content = resp.content.decode('utf-8')
        cvs_reader = csv.reader(io.StringIO(content))
        body = list(cvs_reader)

        self.assertEqual(len(body), 2)
        # most recently updated should be first
        first = body[0]
        self.assertCountEqual([self.tweet2.tweet_id,
                               self.tweet2.text,
                               str(self.tweet2.created_at),
                               str(self.tweet2.sentiment)], first)
        second = body[1]
        self.assertCountEqual([self.tweet1.tweet_id,
                               self.tweet1.text,
                               str(self.tweet1.created_at),
                               str(self.tweet1.sentiment)], second)


    def test_fetch_with_specific_number(self):
        resp = self.client.get(reverse('csv')+'?number=1')
        self.assertEqual(resp.status_code, 200)
        content = resp.content.decode('utf-8')
        cvs_reader = csv.reader(io.StringIO(content))
        body = list(cvs_reader)

        self.assertEqual(len(body), 1)
