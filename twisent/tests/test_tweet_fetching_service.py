from unittest.mock import patch, MagicMock
from django.test import TestCase
from twisent.models import Tweet
from datetime import datetime

from twisent.services.tweet_fetching_service import TweetFetchingService



class TweetFetchingServiceTest(TestCase):

    @patch('twisent.services.tweet_fetching_service.'
           'tweepy.Cursor.items')
    def test_fetching_single_tweet(self, mock_cursor,):
        mock_tweet = MagicMock()
        mock_tweet.text = 'abc'
        mock_tweet.created_at=datetime.now()
        mock_tweet.id = '123'
        mock_cursor.return_value = [mock_tweet]
        TweetFetchingService.fetch_tweets(tags_of_interest=['tag1'],
                                          after_id='1',
                                          num_to_fetch=5,
                                          after_date=datetime.today().date())
        tweet = Tweet.objects.get(tweet_id=mock_tweet.id)
        self.assertEqual(tweet.text, mock_tweet.text)
        self.assertEqual(tweet.created_at, mock_tweet.created_at)


