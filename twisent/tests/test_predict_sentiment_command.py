from unittest.mock import patch
from django.test import TestCase
from twisent.models import Tweet

from twisent.management.commands.predict_sentiment import Command
from datetime import datetime


class PredictSentimentCommandTest(TestCase):

    @patch('twisent.management.commands.predict_sentiment.'
           'PredictionService.predict')
    def test_sending_unprocessed_tweets_for_prediction(self, mock_predict):
        expected_sentiment = 0.5
        mock_predict.return_value = expected_sentiment
        tweet = Tweet(text='foo bar', tweet_id='123', created_at=datetime.now())
        tweet.save()
        Command._send_unprocessed_tweets_for_prediction()
        mock_predict.assert_called_with(tweet.text)
        tweet = Tweet.objects.get(id=tweet.id)
        self.assertEqual(tweet.sentiment, expected_sentiment)
        self.assertEqual(tweet.status, Tweet.Status.PROCESSED)