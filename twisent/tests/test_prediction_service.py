from unittest.mock import patch

import tensorflow as tf
from django.test import TestCase
from twisent.services.prediction_service import PredictionService


class PredictionServiceTest(TestCase):

    # This test is a bit too aware of class internals
    # I would prefer setting up a small real model and a small real dictionary
    # and verifying the flow worked.
    # But for now, this covers that the flow is followed.
    @patch('twisent.services.prediction_service.'
           'PredictionService._replace_reserved_sequences_with_tokens',
           wraps=PredictionService._replace_reserved_sequences_with_tokens)
    @patch('twisent.services.prediction_service.'
           'PredictionService._transform_tokenized_to_input',
           wraps=PredictionService._transform_tokenized_to_input)
    @patch('twisent.services.prediction_service.'
           'PredictionService.tokenizer.tokenize',
           wraps=PredictionService.tokenizer.tokenize)
    @patch('twisent.services.prediction_service.'
           'PredictionService.model.predict',
           wraps=PredictionService.model.predict)
    def test_predict_tweet_applies_preprocessing(self,
                                                 mock_model,
                                                 mock_tokenizer,
                                                 mock_token_to_input,
                                                 mock_reserve
                                                 ):
        PredictionService.w2i = {
            'something': 5,
            PredictionService.out_of_dict_token: 1}
        sentiment = PredictionService.predict('something')
        mock_reserve.assert_called_once_with('something')
        mock_tokenizer.assert_called_once_with('something')
        mock_token_to_input.assert_called_once_with(PredictionService.w2i,
                                                    ['something'])
        mock_model.assert_called_once_with(
            tf.convert_to_tensor([[5]], dtype=tf.int64))
        pass

    def test_reserved_token_replacement(self):
        string_with_reserved_terms = '@somenamehere went on www.google.com' \
                                     ' and found a bargain, socks for 9.99 which is exactly +9.99' \
                                     ' above what they had in pocket, their reaction was going from' \
                                     ' :D to :-) to :| to :-( #winning #losing'
        expected_result = '<user> went on <url> and found a bargain, socks' \
                          ' for <number> which is exactly <number> above what they had in' \
                          ' pocket, their reaction was going from <lolface> to <smileface>' \
                          ' to <neutralface> to <sadface> <hashtag> <hashtag>'
        transformed = PredictionService._replace_reserved_sequences_with_tokens(
            string_with_reserved_terms
        )
        self.assertEqual(transformed, expected_result)

    def test_token_to_index_mapper(self):
        w2i = {
            'foo': 1,
            'bar': 2,
            PredictionService.out_of_dict_token: 10
        }
        tokens = ['bar', 'foo', 'bla']
        transformed = PredictionService._transform_tokenized_to_input(w2i,
                                                                      tokens)
        self.assertCountEqual(transformed,
                              [2, 1, 10])
