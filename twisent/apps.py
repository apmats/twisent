from django.apps import AppConfig


class TwisentConfig(AppConfig):
    name = 'twisent'
