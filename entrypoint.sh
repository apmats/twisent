#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

if [ ! -d "./prediction-assets" ]
then
    mkdir prediction-assets
    wget -O './prediction-assets/w2i.json' 'https://drive.google.com/uc?export=view&id=1FYpS0Wv9h09N1Utr--WRBkc8C9DDEpOh'
    wget -O './prediction-assets/model.h5' 'https://drive.google.com/uc?export=view&id=1xdFpQ0n4vYbEVUqsj-1_r6dFnaGV0U9g'
fi

#python manage.py flush --no-input
python manage.py makemigrations
python manage.py migrate

exec "$@"