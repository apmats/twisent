import os

# Django specific config

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = os.environ.get('SECRET_KEY')
DEBUG = bool(os.environ.get('DJANGO_DEBUG', True))
ALLOWED_HOSTS = ['127.0.0.1']

INSTALLED_APPS = [
    'twisent.apps.TwisentConfig',
]

ROOT_URLCONF = 'django-app.urls'
WSGI_APPLICATION = 'django-app.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('SQL_ENGINE', 'django.db.backends.sqlite3'),
        'NAME': os.environ.get('SQL_DATABASE', os.path.join(BASE_DIR, 'db.sqlite3')),
        'USER': os.environ.get('SQL_USER', 'user'),
        'PASSWORD': os.environ.get('SQL_PASSWORD', 'password'),
        'HOST': os.environ.get('SQL_HOST', 'localhost'),
        'PORT': os.environ.get('SQL_PORT', '5432'),
    }
}

# App specific config

TAGS_OF_INTEREST = ['#trump', '#Trump''#biden', '#Biden', '#elections']
TWEETS_PER_FETCH = 1000
FETCHING_INTERVAL_SECONDS = 10
PROCESSING_INTERVAL_SECONDS = 1
TWITTER_API_KEY = os.environ.get('TWITTER_API_KEY', None)
TWITTER_API_SECRET_KEY = os.environ.get('TWITTER_API_SECRET_KEY', None)
TWITTER_ACCESS_TOKEN = os.environ.get('TWITTER_ACCESS_TOKEN', None)
TWITTER_ACCESS_TOKEN_SECRET = os.environ.get('TWITTER_ACCESS_TOKEN_SECRET', None)

W2I_LOCATION = 'prediction-assets/w2i.json'
MODEL_LOCATION = 'prediction-assets/model.h5'

