from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from django.urls import include
from django.views.generic import RedirectView

urlpatterns = []
urlpatterns += [
    path('twisent/', include('twisent.urls')),
]

# Add URL maps to redirect the base URL to our application
urlpatterns += [
    path('', RedirectView.as_view(url='/twisent', permanent=True)),
]



